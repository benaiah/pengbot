(local handlers (require :handlers))

;; override some handlers and introduce some new ones
;; get and set require io.open, not the nodemcu API

(fn ok-term? [term] (and (: term :match "^[-%w]+$") term))
(fn term-name [term] (.. "terms/" term))

(global file nil) (global gpio nil) (global net nil) (global tmr nil)
(fn handlers.set [_ _ args]
  (let [(term data) (: args :match "^(%S+) (.*)")
        f (file.open (term-name term) :w)]
    (if f
        (do (doto f
              (: :write data)
              (: :close))
            (.. "OK, " term " is now " data))
        "Could not write term for some reason.")))

(fn handlers.get [_ _ term]
  (.. term " is " (if (file.exists (term-name term))
                      (let [file (file.open (term-name term))
                            value (file.read)]
                        (file.close)
                        value)
                      "not something I know about.")))

(fn handlers.on [_ _ args]
  (let [pin (tonumber args)]
    (when pin
      (gpio.mode pin gpio.OUTPUT)
      (gpio.write pin gpio.HIGH)
      (.. "Turned " pin " on."))))

(fn handlers.off [_ _ args]
  (let [pin (tonumber args)]
    (when pin
      (gpio.mode pin gpio.OUTPUT)
      (gpio.write pin gpio.LOW)
      (.. "Turned " pin " off."))))

(fn handlers.read [_ _ args]
  (let [pin (tonumber args)]
    (when pin
      (gpio.mode pin gpio.INPUT)
      (.. "Pin " pin " is "
          (if (= (gpio.read pin) gpio.HIGH) "on" "off")))))

(fn handlers.heap [] (.. "I have " (node.heap) " bytes left."))

(fn handlers.eval []
  "I am only a lowly micropeng; I can't do that. Try regular pengbot!")


;; replace all the luasocket-specific bits in the irc module with nodemcu stuff
(set package.loaded.socket {}) ; avoid require error
(local irc (require :irc))

(fn receive [conn socket data]
  (irc.flush conn)
  (each [line (: data :gmatch "[^\r\n]+")]
    (when line
      (when _G.debug? (print "<" line))
      (let [(prefix command-name rest) (irc.tokenize line)
            command-name (and command-name (: command-name :lower))
            command (. irc.commands command-name)]
        (when command
          (command conn prefix rest))))))

(fn connect [host port nick channels]
  (let [conn {:queue [] :top 1 :bottom 1 :socket (net.createConnection)
              :nick nick :channels channels}
        flusher (tmr.create)]
    (: conn.socket :connect (or port 6667) host)
    (: conn.socket :on :receive (partial receive conn))
    (: conn.socket :on :connection (fn []
                                     (irc.nick conn nick)
                                     (irc.user conn "pengbot" "0" "*"
                                               "A bot written in fennel.")
                                     (print "Connected!")))
    (: conn.socket :on :disconnection (fn [x err]
                                        (print "oh no" x err)
                                        (: flusher :stop)
                                        (print "Reconnecting...")
                                        (connect host port nick channels)))
    (: flusher :alarm 128 tmr.ALARM_AUTO (partial irc.flush conn))
    conn))
