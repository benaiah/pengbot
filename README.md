# P'eng bot

> In the northern ocean there is a fish, called the k'un, I do not
> know how many thousand li in size. This k'un changes into a bird,
> called the p'eng. Its back is I do not know how many thousand li in
> breadth. When it is moved, it flies, its wings obscuring the sky
> like clouds.
>
> When on a voyage, this bird prepares to start for the Southern
> Ocean, the Celestial Lake. And in the Records of Marvels we read
> that when the p'eng flies southwards, the water is smitten for a
> space of three thousand li around, while the bird itself mounts upon
> a great wind to a height of ninety thousand li, for a flight of six
> months' duration.

An IRC bot written in [Fennel][fennel].

Pengbot can run either on a conventional computer or on a
microcontroller; see "Micropeng" below.

## Usage

    $ ./fennel pengbot.fnl chat.freenode.net 6667 pengbot "#pengbot" "#otherchan"

Commands are prefixed with a comma or can be addressed directly to the
bot's nick. Run `,help` to get a list of commands. The main commands are:

* `echo TEXT`: echo back the text
* `set TERM DEFINITION`: store a term and its definition
* `get TERM`: retrieve a term's definition
* `forget TERM`: delete a term's definition
* `eval FORM`: evaluate a piece of Fennel code and print return value

Stores dictionary data (from `set` command) in the `data/` directory
that it's launched from. No limits are in place to prevent it from
filling up the disk yet, so disable that command if you're worried.

Requirements: Lua 5.1+ and [luasocket][luasocket].

## Micropeng

Pengbot can also run on a [NodeMCU][nodemcu] device such as the US$3
[Wemos D1 Mini][d1-mini].

You'll need to configure wifi on your device before you start. Open a
Lua repl and run:

    > wifi.setmode(wifi.STATION)
    > wifi.sta.config({ssid="cybre-netwerk", pwd="reindeerflotilla"})

If you don't have a [compatible NodeMCU build][fw] on your device, you may
need to flash that first:

    $ esptool.py --port /dev/ttyUSB0  write_flash -fm dio 0x00000 nodemcu-master-13-modules-2019-02-04-04-33-08-float.bin 

Install `nodemcu-uploader` to allow uploading:

    $ pip install --user nodemcu-uploader

Ensure the directory that `pip` installs to is on your `$PATH`; on
some systems this is `$HOME/.local/bin`.

Finally, upload the `pengbot` code to the device:

    $ make upload NICK=micropeng2 CHANNEL=mychan

When pengbot is running on NodeMCU, the `on` and `off` commands can
toggle gpio pins. However, in this mode the `eval` command is
unavailable due to memory limitations.

## License

Copyright © 2018-2019 Phil Hagelberg and contributors

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

[fennel]: https://github.com/bakpakin/Fennel
[luasocket]: http://w3.impa.br/~diego/software/luasocket/
[nodemcu]: https://nodemcu.readthedocs.io/
[d1-mini]: https://wiki.wemos.cc/products:d1:d1_mini
[fw]: https://p.hagelb.org/nodemcu-master-13-modules-2019-02-04-04-33-08-float.bin 
