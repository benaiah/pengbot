SERVER := irc.freenode.net
PORT := 6667
NICK := micropeng
CHANNEL := pengbot

USB := /dev/ttyUSB0
FNL=init.fnl micropeng.fnl irc.fnl handlers.fnl
LUA=$(patsubst %.fnl,%.lua,$(FNL))
DEVICE=$(patsubst %.lua,device/%.lua,$(LUA))

run: # to run locally, not on a device
	./fennel pengbot.fnl $(SERVER) $(PORT) $(NICK) $(CHANNEL)

%.lua : %.fnl
	SERVER=$(SERVER) PORT=$(PORT) NICK=$(NICK) CHANNEL=$(CHANNEL) \
		./fennel --compile $^ > $@

device/%.lua: %.lua
	nodemcu-uploader --verbose --port $(USB) upload $^
	touch $@

upload: $(DEVICE)

clean:
	rm $(LUA)

update-fennel: ../fennel/fennel ../fennel/fennel.lua ../fennel/fennelview.fnl
	cp $^ .
