(local init-env {:pairs pairs :ipairs ipairs
                 :tostring tostring :tonumber tonumber
                 :unpack unpack :select select :assert assert :error error
                 :next next :type type :_VERSION _VERSION})

;; copy these in so they can't modify the originals
(each [_ mod (ipairs [:table :math :string :coroutine])]
  (tset init-env mod {})
  (each [k v (pairs (. _G mod))]
    (tset (. init-env mod) k v)))

(global env (or env (let [e {}]
                      (each [k v (pairs init-env)]
                        (tset e k v))
                      e)))

;; prevent directory traversal
(fn ok-term? [term] (and (: term :match "^[-%w]+$") term))

(fn get-term [term]
  (let [f (io.open (.. "data/" (assert (ok-term? term) "bad!")) "rb")]
       (if f
           (let [data (: f :read "*l")]
                (: f :close)
                data)
           "not something I know about.")))

(fn set-term [term data]
  (let [f (io.open (.. "data/" (assert (ok-term? term) "bad!")) "w")]
       (if f
           (do (: f :write data)
               (: f :close)
               (.. "OK, " term " is now " data))
           "Could not write term for some reason.")))

{:botsnack (fn [] "Yum; my favorite!")
 :source (fn [] "https://git.sr.ht/~technomancy/pengbot")
 :author (fn [] "technomancy (https://technomancy.us)")
 :ping (fn [] "pong!")
 :help (fn []
         (let [keys []]
           (each [k (pairs (require "handlers"))] (table.insert keys k))
           (.. "Commands are " (table.concat keys ", ") ".")))
 :echo (fn [_ _ args] args)
 :set (fn [_ _ args]
        (let [(term data) (: args :match "^(%S+) (.*)")]
          (set-term term data)))
 :get (fn [_ _ term]
        (.. term " is " (get-term term)))
 :forget (fn [_ _ term]
           (assert (os.remove (.. "data/" (assert (ok-term? term) "bad!"))))
           "OK, forgot it.")
 :reinit (fn []
           (each [k (pairs env)]
             (tset env k nil))
           (each [k v (pairs init-env)]
             (tset env k v))
           "Reinitialized eval environment.")
 :reload (fn [_ _ module-name]
           (let [mod (require module-name)]
             (tset package.loaded module-name nil)
             (each [k v (pairs (require module-name))]
               (tset mod k v))
             (tset package.loaded module-name mod))
           "OK, reloaded")
 :debug (fn []
          (global debug? (not debug?))
          (if debug? "Debug enabled." "Debug disabled."))
 :compile (fn [_ _ args]
            (let [fennel (require :fennel)
                  fennelview (require :fennelview)
                  (ok val) (pcall fennel.compileString args {:indent ""})
                  oneline-val (when ok (string.gsub val "\n" " "))]
              (if ok oneline-val (.. "Error" val))))
 :eval (fn [_ _ args]
         (set env._G env)
         (let [fennel (require :fennel)
               fennelview (require :fennelview)
               (ok val) (pcall fennel.eval args { :env env })]
           (if ok
               (fennelview val)
               (.. "Error" val))))}
