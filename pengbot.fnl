;; allow requiring fnl files in development but gracefully degrade with AOT
(let [(ok fennel) (pcall require :fennel)]
  (when ok (table.insert package.loaders fennel.searcher)))

(local irc (require :irc))
(local luasocket (require :socket))

(global debug? (if (and os (os.getenv "DEBUG")) true false))

(local server (table.remove arg 1))
(local port (tonumber (table.remove arg 1)))
(local nick (table.remove arg 1))
(local channels arg)

(when (not (and server port nick))
  (print "USAGE: pengbot.fnl SERVER PORT NICK CHANNEL [CHANNEL...]")
  (if os (os.exit 1) (error "abort")))

(fn loop [irc conn]
  (irc.flush conn)
  (let [(line err) (: conn.socket :receive "*l")]
    (when (not (= err "closed"))
      (when line
        (when _G.debug? (print "<" line))
        (let [(prefix command-name rest) (irc.tokenize line)
              command-name (and command-name (: command-name :lower))
              command (. irc.commands command-name)]
          (when command
            (command conn prefix rest))))
      (loop irc conn))))

(fn connect [host port nick channels]
  (let [conn {:queue [] :top 1 :bottom 1 :socket (luasocket.tcp)
              :nick nick :channels channels}]
    (assert (: conn.socket :connect host (or port 6667)))
    (: conn.socket :settimeout 0.1)
    (irc.nick conn nick)
    (irc.user conn "pengbot" "0" "*" "A bot written in fennel.")
    (loop irc conn)))

(connect server port nick channels)
