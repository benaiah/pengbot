(local handlers (require :handlers))

(fn send [socket command use-separator? args]
  (var str command)
  (when (> (# args) 0)
    (let [sep (if use-separator?
                  " :"
                  (> (# args) 1)
                  " " "")]
      (set str (.. str " " (table.concat args " " 1 (- (# args) 1))
                   sep (. args (# args))))))
  (when _G.debug? (print ">" str))
  (: socket :send (.. str "\r\n")))

(fn push [conn command use-separator? args]
  (tset conn.queue conn.bottom [command use-separator? args])
  (set conn.bottom (+ conn.bottom 1)))

(fn tokenize [line]
  (let [(_ e1 prefix)  (: line :find "^:(%S+)")
        (_ e2 command) (: line :find "(%S+)" (if e1 (+ e1 1) 1))
        (_ _ rest)     (: line :find "%s+(.*)" (if e2 (+ e2 1) 1))]
    (values prefix command rest)))

(fn flush [conn]
  (when (not (= conn.top conn.bottom))
    (let [[command separator? args] (. conn.queue conn.top)]
      (tset conn.queue conn.top nil)
      (set conn.top (+ conn.top 1))
      (when (= conn.top conn.bottom)
        (set conn.top 1)
        (set conn.bottom 1))
      (send conn.socket command separator? args))))

(local irc {:commands {} :tokenize tokenize :flush flush})

;; define functions for outgoing IRC operations
(each [name separator? (pairs {:nick false :user true :join false :privmsg true
                               :ping false :pong false :part true :kick true})]
  (tset irc name
        (fn [conn ...]
          (push conn (: name :upper) separator? [...]))))

;; commands are like protocol-level handlers, whereas the handlers
;; module is for application-level handlers.
(fn irc.commands.ping [conn prefix rest] (irc.pong conn rest))

(fn irc.commands.privmsg [conn prefix rest]
  (let [chan (: rest :match "(%S+)")
        msg (: rest :match ":(.*)")
        their-nick (: prefix :match "(%S+)!")
        ;; can use comma prefix char or nick mention
        (cmd args) (if (string.find msg "^,")
                       (: msg :match "^,(%S+) ?(.*)")
                       (: msg :match (string.format "^%s: (%%S+) ?(.*)"
                                                    conn.nick)))
        chan (if (: chan :find "^#") chan their-nick)
        handler (. handlers cmd)]
    (when handler
      (let [(ok response) (pcall handler conn chan args)]
        (if (and ok (= (type response) "string"))
            (irc.privmsg conn chan response)
            (not ok)
            (print "Error in handler" response))))))

;; after you've identified and are properly connected
(fn irc.commands.376 [conn prefix rest]
  (each [_ channel (ipairs conn.channels)]
    (let [channel (if (: channel :find "^#")
                      channel (.. "#" channel))]
      (irc.join conn channel))))

irc
