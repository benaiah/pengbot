;; a launcher for pengbot to make it run on nodemcu
;; takes settings at compile-time and creates an init script from them

(local server (eval-compiler (.. "\"" (os.getenv "SERVER") "\"")))
(local port (eval-compiler (.. "\"" (os.getenv "PORT") "\"")))
(local nick (eval-compiler (.. "\"" (os.getenv "NICK") "\"")))
(local channel (eval-compiler (.. "\"" (os.getenv "CHANNEL") "\"")))

;; don't attempt to connect to IRC until after the wifi has connected
(let [t (tmr.create)]
  (: t :alarm 4096 tmr.ALARM_SEMI
     (fn []
       (local micropeng (require :micropeng))
       (if (= wifi.STA_GOTIP (wifi.sta.status))
           (micropeng server port nick [channel])
           (tmr.start t)))))

